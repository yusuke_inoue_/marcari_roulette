﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Text;
using System;

public class GameController : MonoBehaviour
{
	public AudioSource nextAudioSource;
	public AudioSource finishAudioSource;

	public Text NoteText;
	public Text TitleText;

	public GameObject Roulette1;
	public Text RouletteText1;
	public GameObject Roulette2;
	public Text RouletteText2;
	public GameObject Roulette3;
	public Text RouletteText3;

	public GameObject startButton;
	public GameObject stopButton;
	public GameObject resetButton;

	//private Color ImageYellow = new Color(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f);
	//private Color TextBlack = new Color(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f, 255.0f / 255.0f);

	//private Color ImageGray = new Color(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f, 255.0f / 255.0f);
	//private Color TextGray = new Color(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f, 255.0f / 255.0f);

	private Color[] imageColors = new Color[] { Color.yellow, Color.white, Color.white };
	private Color[] textColors = new Color[] { Color.black, Color.gray, Color.gray };

	private double TimeGap;
	private double TimeCount;

	private int colorCountStay = 0;
	private int colorCountDown = 0;
	
	public bool isPlaying = false;
	public bool isStopping = false;

	void Start()
	{
		stopButton.SetActive(false);
		resetButton.SetActive(false);
	}

	void Update()
	{
		if (isPlaying)
		{
			if (isStopping)
			{
				TimeCount -= Time.deltaTime;

				if (TimeCount <= 0)
				{
					Roulette1.GetComponent<Image>().color = imageColors[(colorCountStay + 3) % 3];
					Roulette2.GetComponent<Image>().color = imageColors[(colorCountStay + 2) % 3];
					Roulette3.GetComponent<Image>().color = imageColors[(colorCountStay + 1) % 3];

					RouletteText1.GetComponent<Text>().color = textColors[(colorCountStay + 3) % 3];
					RouletteText2.GetComponent<Text>().color = textColors[(colorCountStay + 2) % 3];
					RouletteText3.GetComponent<Text>().color = textColors[(colorCountStay + 1) % 3];

					colorCountStay = colorCountStay + 1;
					colorCountDown = colorCountDown + 1;
					TimeCount = colorCountDown * 0.1 + TimeGap;

					nextAudioSource.Play();

				}

				if (TimeCount > 1)
				{
					isStopping = false;
					isPlaying = false;

					resetButton.SetActive(true);

					int result = colorCountStay % 3;

					finishAudioSource.Play();

					if (result == 1)
					{
						TitleText.text = "100円OFF！";
					}
					else if(result == 2)
					{
						TitleText.text = "200円OFF！";
					}
					else if(result == 0)
					{
						TitleText.text = "300円OFF！";
					}

				}
			}
			else
			{
				TimeCount -= Time.deltaTime;

				if (TimeCount <= 0)
				{
					Roulette1.GetComponent<Image>().color = imageColors[(colorCountStay + 3) % 3];
					Roulette2.GetComponent<Image>().color = imageColors[(colorCountStay + 2) % 3];
					Roulette3.GetComponent<Image>().color = imageColors[(colorCountStay + 1) % 3];

					RouletteText1.GetComponent<Text>().color = textColors[(colorCountStay + 3) % 3];
					RouletteText2.GetComponent<Text>().color = textColors[(colorCountStay + 2) % 3];
					RouletteText3.GetComponent<Text>().color = textColors[(colorCountStay + 1) % 3];

					nextAudioSource.Play();

					colorCountStay = colorCountStay + 1;
					TimeCount = TimeGap;
				}
			}
		}
	}

	public void RouletteStart()
	{
		isPlaying = true;
		NoteText.text = "";

		TimeGap = 0.1;
		TimeCount = TimeGap;

		TitleText.text = "";

		startButton.SetActive(false);
		stopButton.SetActive(true);

		//Roulette1.GetComponent<Image>().color = Color.red;
		//RouletteText1.GetComponent<Text>().color = TextBlack;

		//Roulette2.GetComponent<Image>().color = ImageGray;
		//RouletteText2.GetComponent<Text>().color = TextGray;

		//Roulette3.GetComponent<Image>().color = ImageGray;
		//RouletteText3.GetComponent<Text>().color = TextGray;
	}

	public void RouletteStop()
	{
		isStopping = true;

		stopButton.SetActive(false);

		TimeCount = 0.5;
	}

	public void RouletteReset()
	{
		resetButton.SetActive(false);
		startButton.SetActive(true);

		colorCountStay = 0;
		colorCountDown = 0;

		//SceneManager.LoadScene("marcariGame");

		NoteText.text = "※音量を上げてください";
		TitleText.text = "ルーレット";

		Roulette1.GetComponent<Image>().color = imageColors[2];
		Roulette2.GetComponent<Image>().color = imageColors[2];
		Roulette3.GetComponent<Image>().color = imageColors[2];

		RouletteText1.GetComponent<Text>().color = textColors[0];
		RouletteText2.GetComponent<Text>().color = textColors[0];
		RouletteText3.GetComponent<Text>().color = textColors[0];

	}
}

//ボイス スタート、ストップ、選択肢3
